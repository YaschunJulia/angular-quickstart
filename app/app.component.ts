import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `<h1 id="title">My {{number}} Angular App</h1>
  <button id="btn" (click)="change()">Change title</button>`
})
export class AppComponent implements OnInit {
  number: string;

  ngOnInit() {
    this.number = "First";
  }

  change() {
    this.number = "Second";
  }

}