"use strict";
/**
 * Created by juliayaschun on 02.03.17.
 */
var testing_1 = require('@angular/core/testing');
var platform_browser_1 = require('@angular/platform-browser');
var app_component_1 = require('./app.component');
var comp;
var fixture;
describe('AppComponent', function () {
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [app_component_1.AppComponent],
            providers: [],
            imports: [platform_browser_1.BrowserModule],
        })
            .compileComponents().then(createComponent);
    }));
    it('should create', function () {
        expect(comp).toBeTruthy();
    });
    it('should be right title', function () {
        var change = spyOn(app_component_1.AppComponent.prototype, 'change').and.callThrough();
        var oldTitle = fixture.elementRef.nativeElement.querySelector('#title').innerText;
        var btn = fixture.elementRef.nativeElement.querySelector('#btn');
        var e = new MouseEvent('click');
        btn.dispatchEvent(e);
        expect(change).toHaveBeenCalled();
        fixture.detectChanges();
        fixture.whenStable().then(function () {
            fixture.detectChanges();
            var newTitle = fixture.elementRef.nativeElement.querySelector('#title').innerText;
            expect(newTitle).not.toEqual(oldTitle);
        });
    });
});
function createComponent() {
    fixture = testing_1.TestBed.createComponent(app_component_1.AppComponent);
    comp = fixture.componentInstance;
    // change detection triggers ngOnInit which gets a hero
    fixture.detectChanges();
    return fixture.whenStable().then(function () {
        fixture.detectChanges();
    });
}
//# sourceMappingURL=app.component.spec.js.map