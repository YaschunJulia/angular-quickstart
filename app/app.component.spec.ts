/**
 * Created by juliayaschun on 02.03.17.
 */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { By }              from '@angular/platform-browser';
import { DebugElement }    from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

let comp: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe('AppComponent', function () {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent],
            providers: [],
            imports: [BrowserModule],
        })
            .compileComponents().then(createComponent);
    }));

    it('should create', () => {
        expect(comp).toBeTruthy();
    });

    it('should be right title', () => {
        let change = spyOn(AppComponent.prototype, 'change').and.callThrough();

        let oldTitle = fixture.elementRef.nativeElement.querySelector('#title').innerText;
        let btn = fixture.elementRef.nativeElement.querySelector('#btn');

        let e = new MouseEvent('click');

        btn.dispatchEvent(e);

        expect(change).toHaveBeenCalled();

        fixture.detectChanges();
        fixture.whenStable().then(() => {
            fixture.detectChanges();
            let newTitle = fixture.elementRef.nativeElement.querySelector('#title').innerText;
            expect(newTitle).not.toEqual(oldTitle);
        });
    });
});

function createComponent() {
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;

    // change detection triggers ngOnInit which gets a hero
    fixture.detectChanges();

    return fixture.whenStable().then(() => {
        fixture.detectChanges();
    });
}
